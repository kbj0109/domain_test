const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const chalk = require("chalk");
// https://jsp-dev.tistory.com/entry/Express-letsencrypt%EB%A5%BC-%EC%82%AC%EC%9A%A9%ED%95%98%EC%97%AC-https-%EC%A0%81%EC%9A%A9%ED%95%98%EA%B8%B0
// https://harrythegreat.tistory.com/entry/Let%E2%80%99s-Encrypt-nodejs-%EC%A0%81%EC%9A%A9%EA%B8%B0
// https://velog.io/@alskt0419/Node.js-%EC%89%BD%EA%B2%8C-https-%EC%A0%81%EC%9A%A9-%EC%8B%9C%ED%82%A4%EB%8A%94-%EB%B2%95

const green = message => console.log(chalk.green(message));
const red = message => console.log(chalk.red(message));
const yellow = message => console.log(chalk.yellow(message));

const app = express();
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("views")); //정적 파일 접근을 위한 경로 지정
app.set("view engine", "pug");
app.use(cookieParser("CookiePassword"));

app.listen(80, () => {
  green("Server is waiting at http://localhost:80");
});

app.listen(2000, () => {
  green("Server is waiting at http://localhost:2000");
});

app.get("/", (req, res) => {
  green("** / 들어옴");

  yellow(req.signedCookies.signedCookie___);
  yellow(req.cookies.unsignedCookie___);

  res.cookie("signedCookie___", "This Is Signed", { signed: true }); //쿠키 암호화 하기
  res.cookie("unsignedCookie___", "This Is NotSigned");
  res.cookie("httpOnlyCookie", "This is HttpOnly Cookie", { httpOnly: true });
  res.cookie("secureCookie", "This is Secure Cookie", { secure: true });
  res.cookie("pathCookie", "This is Path Cookie", { path: "/test1" });

  res.render("index");
});
app.get("/test1", (req, res) => {
  green("** / test1 들어옴");
  res.render("test1");
});
app.get("/test2", (req, res) => {
  green("** / test2 들어옴");
  res.render("test2");
});

/** ************************************************************** */
/** ************************************************************** */
/** ************************************************************** */
/** ************************************************************** */

if (process.env.NODE_ENV === "production") {
  const https = require("https");
  const sslPath = "/etc/letsencrypt/live/kbj0109.com";
  const sslPort = 443;

  const path = require("path");
  const fs = require("fs");

  try {
    const option = {
      ca: fs.readFileSync(`${sslPath}/fullchain.pem`),
      key: fs
        .readFileSync(
          path.resolve(process.cwd(), `${sslPath}/privkey.pem`),
          "utf8"
        )
        .toString(),
      cert: fs
        .readFileSync(
          path.resolve(process.cwd(), `${sslPath}/cert.pem`),
          "utf8"
        )
        .toString()
    };

    https.createServer(option, app).listen(sslPort, () => {
      green(`[https] Server is started on port ${sslPort}`);
    });
  } catch (error) {
    red("[https] https 오류가 발생하였습니다. https 서버는 실행되지 않습니다.");
    red(error);
  }
} else {
  green(chalk.green("This is Development, No Https "));
}
